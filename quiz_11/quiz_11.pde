int n = 10;
Walker[] walkers = new Walker[n];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  resetEverything();
}

void resetEverything()
{
  for (int i = 0; i < n; i++)
  {
    walkers[i] = new Walker();
    walkers[i].mass = random(1, 10);
    walkers[i].scale = walkers[i].mass * 15;
    walkers[i].changeColor();
    walkers[i].position = new PVector(random(Window.left, Window.right), 
                                      random(Window.bottom, Window.top));
  }
}

void mousePressed()
{
  resetEverything();
}

void draw()
{
  background(255);
  noStroke();
  
  for (Walker i : walkers)
  {
    i.render();
    i.update();
    
    for (Walker j : walkers)
    {
      if (i != j)
      {
        i.applyForce(j.calculateAttraction(i));
      }
    }
  }
}
