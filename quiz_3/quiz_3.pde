int frames = 0;

void setup()
{
  size(1020, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background(255);
}

void draw()
{
  if (frames % 300 == 0)
  {
    clear();
    background(255);
  }

  paintSplatter(150, 0, 30, 5);

  frames++;
}

void paintSplatter(float xDeviation, float xMean, float scaleDeviation, float scaleMean)
{
  float gaussian = randomGaussian();

  float x = xDeviation * gaussian + xMean;
  float y = random(-height/2, height/2);

  gaussian = randomGaussian(); // new value

  float scale = scaleDeviation * gaussian + scaleMean;

  int r = int(random(256));
  int g = int(random(256));
  int b = int(random(256));
  int a = int(random(10, 101));

  noStroke();
  fill(r, g, b, a);
  circle(x, y, scale);
}
