class Walker
{
  float x;
  float y;
  color col;

  void render()
  {
    fill(col);
    noStroke();
    circle(x, y, 30);
  }

  void randomWalk(int movespace)
  {
    int rng = int(random(8));

    if (rng == 0)
    {
      y+=movespace;
    } else if (rng == 1)
    {
      y-=movespace;
    } else if (rng == 2)
    {
      x+=movespace;
    } else if (rng == 3)
    {
      x-=movespace;
    } else if (rng == 4)
    {
      y+=movespace;
      x+=movespace;
    } else if (rng == 5)
    {
      y+=movespace;
      x-=movespace;
    } else if (rng == 6)
    {
      y-=movespace;
      x+=movespace;
    } else if (rng == 7)
    {
      y-=movespace;
      x-=movespace;
    }
  }

  void randomWalkBiased(float movespace)
  {
    int rng = int(random(100));

    if (rng < 15)
    {
      y+=movespace;
    } else if (rng < 30)
    {
      y-=movespace;
    } else if (rng < 45)
    {
      x+=movespace;
    } else if (rng < 100)
    {
      x-=movespace;
    }
  }
  
  void changeColor()
  {
    int r = int(random(256));
    int g = int(random(256));
    int b = int(random(256));
    int a = int(random(50, 101));
    
    col = color(r, g, b, a);
  }
}
