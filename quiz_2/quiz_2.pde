void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

Walker johnnieWalker = new Walker();
Walker plWalker = new Walker();

void draw()
{
  johnnieWalker.randomWalk(10);
  johnnieWalker.changeColor();
  johnnieWalker.render();
  
  plWalker.randomWalkBiased(5);
  plWalker.changeColor();
  plWalker.render();
}
