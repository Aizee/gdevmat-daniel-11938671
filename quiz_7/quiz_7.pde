Walker[] walkers = new Walker[100];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < 100; i++)
  {
    walkers[i] = new Walker();
    walkers[i].scale = int(random(10, 30));
    walkers[i].position = new PVector(random(Window.left, Window.right), 
                                      random(Window.bottom, Window.top));
  }
}

PVector mousePos()
{
  float x = mouseX - Window.w;
  float y = -(mouseY - Window.h);
  return new PVector(x, y);
}

void draw()
{
  background(80);
  
  for (int i = 0; i < 100; i++)
  {
    PVector direction = PVector.sub(mousePos(), walkers[i].position).normalize();
    walkers[i].acceleration = direction.mult(0.2);
    
    walkers[i].update();
    walkers[i].render();
  }
}
