void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}

void draw()
{
  background(130);
  
  PVector mouse = mousePos();
  
  mouse.normalize();
  
  strokeWeight(7);
  stroke(0, 255, 0);
  PVector beam1 = PVector.mult(mouse, 200);
  line(-beam1.x, -beam1.y, beam1.x, beam1.y);
  
  println(beam1.mag()); // magnitude of one side
  
  strokeWeight(5);
  stroke(255, 255, 255);
  line(-beam1.x, -beam1.y, beam1.x, beam1.y);
  
  strokeWeight(7);
  stroke(0);
  PVector handle1 = PVector.mult(mouse, 40);
  line(-handle1.x, -handle1.y, handle1.x, handle1.y);
  
  strokeWeight(7);
  stroke(0, 255, 0);
  PVector perp1 = new PVector(mouse.y, -mouse.x);
  PVector saber2pos = PVector.mult(mouse, 20);
  perp1.mult(50);
  line(-perp1.x+saber2pos.x, -perp1.y+saber2pos.y, perp1.x+saber2pos.x, perp1.y+saber2pos.y);
  
  strokeWeight(5);
  stroke(255, 255, 255);
  line(-perp1.x+saber2pos.x, -perp1.y+saber2pos.y, perp1.x+saber2pos.x, perp1.y+saber2pos.y);
  perp1.div(50);
  
  strokeWeight(7);
  stroke(0);
  perp1.mult(30);
  line(-perp1.x+saber2pos.x, -perp1.y+saber2pos.y, perp1.x+saber2pos.x, perp1.y+saber2pos.y);
  perp1.div(30);
  
  strokeWeight(7);
  stroke(0, 255, 0);
  PVector saber3pos = PVector.mult(mouse, -20);
  perp1.mult(50);
  line(-perp1.x+saber3pos.x, -perp1.y+saber3pos.y, perp1.x+saber3pos.x, perp1.y+saber3pos.y);
  
  strokeWeight(5);
  stroke(255, 255, 255);
  line(-perp1.x+saber3pos.x, -perp1.y+saber3pos.y, perp1.x+saber3pos.x, perp1.y+saber3pos.y);
  perp1.div(50);
  
  strokeWeight(7);
  stroke(0);
  perp1.mult(30);
  line(-perp1.x+saber3pos.x, -perp1.y+saber3pos.y, perp1.x+saber3pos.x, perp1.y+saber3pos.y);
  perp1.div(30);
  
  strokeWeight(7);
  stroke(0, 255, 0);
  PVector perp2 = mouse;
  PVector saber4pos = PVector.mult(perp1, 10);
  perp2.mult(100);
  line(-perp2.x+saber4pos.x, -perp2.y+saber4pos.y, perp2.x+saber4pos.x, perp2.y+saber4pos.y);
  
  strokeWeight(5);
  stroke(255, 255, 255);
  line(-perp2.x+saber4pos.x, -perp2.y+saber4pos.y, perp2.x+saber4pos.x, perp2.y+saber4pos.y);
  perp2.div(100);
  
  strokeWeight(7);
  stroke(0);
  perp2.mult(60);
  line(-perp2.x+saber4pos.x, -perp2.y+saber4pos.y, perp2.x+saber4pos.x, perp2.y+saber4pos.y);
  perp2.div(60);
  
  strokeWeight(7);
  stroke(0, 255, 0);
  PVector saber5pos = PVector.mult(perp1, -10);
  perp2.mult(100);
  line(-perp2.x+saber5pos.x, -perp2.y+saber5pos.y, perp2.x+saber5pos.x, perp2.y+saber5pos.y);
  
  strokeWeight(5);
  stroke(255, 255, 255);
  line(-perp2.x+saber5pos.x, -perp2.y+saber5pos.y, perp2.x+saber5pos.x, perp2.y+saber5pos.y);
  perp2.div(100);
  
  strokeWeight(7);
  stroke(0);
  perp2.mult(60);
  line(-perp2.x+saber5pos.x, -perp2.y+saber5pos.y, perp2.x+saber5pos.x, perp2.y+saber5pos.y);
  perp2.div(60);
}
