Walker blackhole = new Walker();
int n = 100;
Walker[] planets = new Walker[n];
int frames = 0;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  spawnBlackhole();
  spawnPlanets();
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}

void draw()
{
  background(15);

  PVector mouse = mousePos();

  if (frames >= 300)
  {
    spawnBlackhole();
    spawnPlanets();
    frames = 0;
  }

  blackhole.succDirection = PVector.sub(mouse, blackhole.position).normalize();
  blackhole.succ(1.5);

  for (int i = 0; i < n; i++)
  {
    if (!planets[i].doNotRender)
    {
      planets[i].render();
      planets[i].succDirection = PVector.sub(blackhole.position, planets[i].position).normalize();
      planets[i].succ(2);
    }
    
    if (PVector.sub(blackhole.position, planets[i].position).mag() <= 0) planets[i].doNotRender = true;
  }


  blackhole.render();
  blackhole.scale = 48;
  blackhole.col = color(0);
  blackhole.render();
  blackhole.scale = 50;
  blackhole.col = color(255);

  frames++;
}

PVector randGauss(float xDev, float yDev, float xMean, float yMean)
{
  float x = xDev * randomGaussian() + xMean;
  float y = yDev * randomGaussian() + yMean;

  return new PVector(x, y);
}

void spawnBlackhole()
{
  blackhole.position = new PVector(int(random(Window.left+blackhole.scale, Window.right-blackhole.scale)), 
    int(random(Window.bottom+blackhole.scale, Window.top-blackhole.scale)));
  blackhole.col = color(255);
}

void spawnPlanets()
{
  for (int i = 0; i < n; i++)
  {
    planets[i] = new Walker();
    planets[i].scale = int(random(10, 50));
    PVector position = randGauss(150, 150, 0, 0);
    position.x = constrain(position.x, Window.left+planets[i].scale, Window.right-planets[i].scale);
    position.y = constrain(position.y, Window.bottom+planets[i].scale, Window.top-planets[i].scale);
    planets[i].position = position;
    planets[i].changeColor();
  }
}
