public class Walker
{
  public float x, y, s;
  public float r, g, b;
  public float tx = 0, ty = 10000, ts = 2000;
  public float tr = 100, tg = 40000, tb = 9000;

  void render()
  {
    noStroke();
    fill(r, g, b);
    circle(x, y, s);
  }

  void perlinWalk()
  {
    x = map(noise(tx), 0, 1, -(width/2f), width/2f);
    y = map(noise(ty), 0, 1, -(height/2f), height/2f);
    s = map(noise(ts), 0, 1, 5, 150);

    tx += 0.001f;
    ty += 0.01f;
    ts += 0.05f;
  }

  void setColor()
  {
    r = map(noise(tr), 0, 1, 0, 255);
    g = map(noise(tg), 0, 1, 0, 255);
    b = map(noise(tb), 0, 1, 0, 255);

    tr += 0.1f;
    tg += 0.01f;
    tb += 0.05f;
  }
}
