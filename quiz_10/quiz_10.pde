int n = 8;
Walker[] walkers = new Walker[n];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  resetEverything();
}

void resetEverything()
{
  int posX = 0;
  for (int i = 0; i < n; i++)
  {
    posX = 2 * (Window.w / n) * (i - (n / 2));
    walkers[i] = new Walker();
    walkers[i].mass = random(1, 10);
    walkers[i].scale = walkers[i].mass * 15;
    walkers[i].changeColor();
    walkers[i].position = new PVector(posX, Window.top + 20);
  }
}

void mousePressed()
{
  resetEverything();
}

void draw()
{
  background(255);

  ocean.render();



  for (Walker w : walkers)
  {
    w.render();
    w.update();

    PVector gravity = new PVector(0, -0.15f * w.mass);
    w.applyForce(gravity);

    float c = 0.1f;
    float normal = 1;
    float frictionMagnitude = c * normal;
    PVector friction = w.velocity.copy();
    // F = -uNv;
    w.applyForce(friction.mult(-1).normalize().mult(frictionMagnitude));

    w.bounceOffEdges();
    
    PVector wind = new PVector(0.1, 0);
    
    if (ocean.isCollidingWith(w))
    {
      PVector dragForce = ocean.calculateDragForce(w);
      w.applyForce(dragForce);
      wind.mult(0);
    }
    
    w.applyForce(wind);
  }
}
