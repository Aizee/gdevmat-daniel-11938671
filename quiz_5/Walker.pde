class Walker
{
  public PVector position = new PVector();
  public PVector tpos = new PVector(0, 10000);
  public PVector speed = new PVector(5, 8);
  public float scale = 50;
  public float ts = 2000;
  public float r, g, b, a;
  public float tr = 100, tg = 40000, tb = 9000;
  public color col;

  void render()
  {
    noStroke();
    fill(col);
    circle(position.x, position.y, scale);
  }

  void randomWalk(int movespace)
  {
    int rng = int(random(8));

    if (rng == 0)
    {
      position.y+=movespace;
    } else if (rng == 1)
    {
      position.y-=movespace;
    } else if (rng == 2)
    {
      position.x+=movespace;
    } else if (rng == 3)
    {
      position.x-=movespace;
    } else if (rng == 4)
    {
      position.y+=movespace;
      position.x+=movespace;
    } else if (rng == 5)
    {
      position.y+=movespace;
      position.x-=movespace;
    } else if (rng == 6)
    {
      position.y-=movespace;
      position.x+=movespace;
    } else if (rng == 7)
    {
      position.y-=movespace;
      position.x-=movespace;
    }
  }

  void randomWalkBiased(float movespace)
  {
    int rng = int(random(100));

    if (rng < 15)
    {
      position.y+=movespace;
    } else if (rng < 30)
    {
      position.y-=movespace;
    } else if (rng < 45)
    {
      position.x+=movespace;
    } else if (rng < 100)
    {
      position.x-=movespace;
    }
  }

  void changeColor()
  {
    r = int(random(256));
    g = int(random(256));
    b = int(random(256));
    a = int(random(50, 101));

    col = color(r, g, b, a);
  }

  void perlinWalk()
  {
    position.x = map(noise(tpos.x), 0, 1, -(width/2f), width/2f);
    position.y = map(noise(tpos.y), 0, 1, -(height/2f), height/2f);
    scale = map(noise(ts), 0, 1, 5, 150);

    tpos.x += 0.001f;
    tpos.y += 0.01f;
    ts += 0.05f;
  }

  void setColor()
  {
    r = map(noise(tr), 0, 1, 0, 255);
    g = map(noise(tg), 0, 1, 0, 255);
    b = map(noise(tb), 0, 1, 0, 255);

    tr += 0.1f;
    tg += 0.01f;
    tb += 0.05f;

    col = color(r, g, b);
  }

  void moveAndBounce()
  {
    position.add(speed);

    if ((position.x > Window.right) || (position.x < Window.left))
    {
      speed.x *= -1;
      changeColor();
    }

    if ((position.y > Window.top) || (position.y < Window.bottom))
    {
      speed.y *= -1;
      changeColor();
    }
  }
}
