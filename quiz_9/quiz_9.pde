int n = 8;
Walker[] walkers = new Walker[n];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  resetEverything();
}

void resetEverything()
{
  int posY = 0;
  for (int i = 0; i < n; i++)
  {
    posY = 2 * (Window.h / n) * (i - (n / 2));
    walkers[i] = new Walker();
    walkers[i].mass = n - i + 2;
    walkers[i].scale = walkers[i].mass * 15;
    walkers[i].changeColor();
    walkers[i].position = new PVector(-400, posY);
  }
}

void mousePressed()
{
  resetEverything();
}

void draw()
{
  background(255);
  strokeWeight(5);
  stroke(0);
  line(0, Window.top, 0, Window.bottom);
  noStroke();

  for (Walker w : walkers)
  {
    // Friction = -1 * mew * N * v
    float mew = 0.01f;
    if (w.position.x >= 0)
    {
      mew = 0.4f;
    }
    
    float normal = 1;
    float frictionMagnitude = mew * normal;
    PVector friction = w.velocity.copy();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    PVector acceleration = new PVector(0.02, 0).mult(w.mass);
    
    w.update();
    w.render();
    w.applyForce(acceleration);
    w.applyForce(friction);
  }
}
