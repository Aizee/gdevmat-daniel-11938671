int time = 0;
int frequency = 10;
int amplitude = 30;

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

void draw()
{
  background(0);

  time ++;

  drawCartesianPlane();
  drawFunction1();
  drawFunction2();
  drawSineWave();
  changeAmplitude();
  changeFrequency();
}

void drawFunction1()
{
  color yellow = color(255, 255, 0);
  fill(yellow);
  noStroke();

  for (float x = -300; x <= 300; x += 0.1f)
  {
    circle(x, ((x * x) - (15 * x) - 3), 5);
  }
}

void drawFunction2()
{
  color purple = color(255, 0, 255);
  fill(purple);
  noStroke();

  for (float x = -300; x <= 300; x += 0.1f)
  {
    circle(x, ((-5 * x) + 30), 5);
  }
}

void drawSineWave()
{
  color cyan = color(0, 255, 255);
  fill(cyan);
  noStroke();

  for (float x = -30; x <= 30; x += 0.01f)
  {
    circle((x * frequency), (float)Math.sin(x + time) * amplitude, 5);
  }
}

void changeAmplitude()
{
  if (keyPressed)
  {
    if (key == 'w' || key == 'W')
    {
      amplitude++;
    }
    else if (key == 's' || key == 'S')
    {
      amplitude--;
    }
  }
}

void changeFrequency()
{
  if (keyPressed)
  {
    if (key == 'a' || key == 'A')
    {
      frequency++;
    }
    else if (key == 'd' || key == 'D')
    {
      frequency--;
    }
  }
}

void drawCartesianPlane()
{
  color white = color(255, 255, 255);
  strokeWeight(1);
  fill(white);
  stroke(white);
  line(-640, 0, 640, 0);
  line(0, -360, 0, 360);

  for (int i = -(width/2); i <= width/2; i+= 10)
  {
    line(i, -5, i, 5);

    if (i >= -(height/2) && i <= height/2)
    {
      line(-5, i, 5, i);
    }
  }
}

void drawLinearFunction()
{
  color red = color(255, 0, 0);
  fill(red);
  noStroke();

  for (int x = -200; x <= 200; x++)
  {
    circle(x, x + 2, 5);
  }
}

void drawQuadraticFunction()
{
  color green = color(0, 255, 0);
  fill(green);
  noStroke();

  for (float x = -300; x <= 300; x += 0.1f)
  {
    circle(x * 10, ((float)Math.pow(x, 2) + (x * 2) - 5), 5);
  }
}

void drawCircle()
{
  color blue = color(0, 0, 255);
  fill(blue);
  stroke(blue);
  float radius = 50;

  for (int x = 0; x <= 360; x++)
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 5);
  }
}
